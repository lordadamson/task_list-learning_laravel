<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use App\Task;
use Illuminate\Http\Request;

/**
 * Display All Tasks
 */
Route::get('/', 'TaskController@displayAllTasks');

/**
 * Add A New Task
 */
Route::post('/task', 'TaskController@addTask');

/**
* get the update task view
*/
Route::get('/task/{id}', 'TaskController@displayTask');

/**
* update task
*/
Route::post('/task/update', 'TaskController@updateTask');

/**
 * Delete An Existing Task
 */
Route::delete('/task/delete/{id}', 'TaskController@deleteTask');