<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use App\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

class TaskController extends Controller
{
    public function addTask(Request $request)
    {
        $validator = Validator::make($request->all(), [
        'name' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return redirect('/')
                ->withInput()
                ->withErrors($validator);
    }

    $task = new Task;
    $task->name = $request->name;
    $task->save();

    return redirect('/');
    }
    
    public function displayAllTasks() {
        $tasks = Task::orderBy('created_at', 'asc')->get();

        return view('tasks', [
            'tasks' => $tasks
        ]);
    }
    
    public function displayTask(Request $request) {
        $task = Task::find($request->id);
        if (empty($task))
            return view('errors.404');

        return view('edit_task', [
            'task' => $task
        ]);
    }
    
    public function updateTask() {
        $taskid = Input::get("id");
        $taskname = Input::get("taskname");

        $validator = Validator::make(Input::all(), [
            'taskname' => 'required|max:255',
        ]);

        if ($validator->fails()) {
                return redirect('/task/'.$taskid)
                    ->withInput()
                    ->withErrors($validator);
        }

        $task = Task::find($taskid);
        $task->name = $taskname;
        $task->save();
        
        return redirect('/');
    }
    
    public function deleteTask($id) {
        Task::findOrFail($id)->delete();

        return redirect('/');
    }
}