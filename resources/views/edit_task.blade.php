@extends('layouts.app')

@section('content')

<div class="panel-body">

@include('common.errors')

	<form action="/task/update" method="POST">
	    <input type="text" name="taskname" value="{{ $task->name }}" id="id" class="form-control">
	    <input type="hidden" name="_token" id="csrf-token" value="{{csrf_token()}}" />
	    <input type="hidden" name='id' value="{{ $task->id }}"/>
	    <button type="submit" class="btn btn-default">
	        <i class="fa fa-plus"></i> Update Task
	    </button>
	</form>

	</div>

@endsection