<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Laravel Quickstart - Basic</title>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- CSS And JavaScript -->
        <link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    </head>

    <body>
        @yield('content')
    </body>
</html>